/// add_blood(x,y,angle)
with(oBloodDecals){
var xx,yy;
surface_set_target(floorBlood)
xx=(argument0-48)*8
yy=(argument1-32)*8

s=0.5+random(1)
draw_sprite_ext(sBloodSplat,0,xx,yy,1+random(0.2),1,argument2,c_white,1)

surface_reset_target()
tex1=surface_get_texture(floorBlood)}
