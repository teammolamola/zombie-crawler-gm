/// draw_borders(width,height)
{
    global.borderM=d3d_model_create()
    
    var w,h;
    w=argument0
    h=argument1

    d3d_model_primitive_begin(global.borderM,pr_trianglestrip) //TOP
    d3d_model_vertex_texture(global.borderM,0,0,0,0,0)
    d3d_model_vertex_texture(global.borderM,0,16,0,0,16/48)
    
    d3d_model_vertex_texture(global.borderM,16,0,0,16/48,0)
    d3d_model_vertex_texture(global.borderM,16,16,0,16/48,16/48)
    
    d3d_model_vertex_texture(global.borderM,w-16,0,0,32/48,0)
    d3d_model_vertex_texture(global.borderM,w-16,16,0,32/48,16/48)
    d3d_model_primitive_end(global.borderM)
    
    d3d_model_primitive_begin(global.borderM,pr_trianglestrip) //RIGHT
    d3d_model_vertex_texture(global.borderM,w,0,0,1,0)
    d3d_model_vertex_texture(global.borderM,w-16,0,0,32/48,0)
    
    d3d_model_vertex_texture(global.borderM,w,16,0,1,16/48)
    d3d_model_vertex_texture(global.borderM,w-16,16,0,32/48,16/48)
    
    d3d_model_vertex_texture(global.borderM,w,h-16,0,1,32/48)
    d3d_model_vertex_texture(global.borderM,w-16,h-16,0,32/48,32/48)
    d3d_model_primitive_end(global.borderM)
    
    d3d_model_primitive_begin(global.borderM,pr_trianglestrip) //BOTTOM
    d3d_model_vertex_texture(global.borderM,w,h,0,1,1)
    d3d_model_vertex_texture(global.borderM,w,h-16,0,1,32/48)
    
    d3d_model_vertex_texture(global.borderM,w-16,h,0,32/48,1)
    d3d_model_vertex_texture(global.borderM,w-16,h-16,0,32/48,32/48)
    
    d3d_model_vertex_texture(global.borderM,16,h,0,16/48,1)
    d3d_model_vertex_texture(global.borderM,16,h-16,0,16/48,32/48)
    d3d_model_primitive_end(global.borderM)
    
    d3d_model_primitive_begin(global.borderM,pr_trianglestrip) //LEFT
    d3d_model_vertex_texture(global.borderM,0,h,0,0,1)
    d3d_model_vertex_texture(global.borderM,16,h,0,16/48,1)
    
    d3d_model_vertex_texture(global.borderM,0,h-16,0,0,32/48)
    d3d_model_vertex_texture(global.borderM,16,h-16,0,16/48,32/48)
    
    d3d_model_vertex_texture(global.borderM,0,16,0,0,16/48)
    d3d_model_vertex_texture(global.borderM,16,16,0,16/48,16/48)
    d3d_model_primitive_end(global.borderM)
}
