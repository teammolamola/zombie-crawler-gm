/// turn_to_page(target)
{
    var current,target;
    current=0
    target=argument0
    //Find current open page.
    for(i=0;i<instance_number(oPage);i+=1){
        if(oMenu.pageC[i].open)
            current+=1
        if(!oMenu.pageC[i].canTurn)return false
    }
    
    if(current<target){
        for(i=0;i<target-current;i+=1){
            turn_page(oMenu.pageC[current+i],true,i*2)
        }
    } else if(current>target){
        for(i=1;i<(current+1)-target;i+=1){
            turn_page(oMenu.pageC[current-i],false,(i-1)*2)
        }
    } else {
        return false
    }
    
    return true
}
