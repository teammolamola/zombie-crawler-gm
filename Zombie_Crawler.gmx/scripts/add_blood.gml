/// add_blood(x,y,num,spread)
with(oBloodDecals){
var xx,yy,sp;
surface_set_target(floorBlood)
xx=(argument0-48)*8
yy=(argument1-32)*8
sp=argument3

repeat(argument2){
s=0.5+random(1)
draw_sprite_ext(sBloodSplat,random(2),xx+random_range(-sp,sp),
yy+random_range(-sp,sp),s,s,random(360),c_white,1)}

surface_reset_target()
tex1=surface_get_texture(floorBlood)}
