var sDll;
sDll="sort.dll"

global.__sort_init=external_define(sDll,"initDll",dll_cdecl,ty_real,0)
global.__sort_free =external_define(sDll,"freeDll",dll_cdecl,ty_real,0)

global.__sort_add_container=external_define(sDll,"addContainer",dll_cdecl,ty_real,2,ty_real,ty_string)
global.__sort_delete_container=external_define(sDll,"deleteContainer",dll_cdecl,ty_real,1,ty_real)
global.__sort_get_size=external_define(sDll,"getSize",dll_cdecl,ty_real,0)

global.__sort_set_container_string=external_define(sDll,"setContainerString",dll_cdecl,ty_real,2,ty_real,ty_string)
global.__sort_set_compare_value=external_define(sDll,"setCompareValue",dll_cdecl,ty_real,2,ty_real,ty_real)

global.__sort_clear=external_define(sDll,"clear",dll_cdecl,ty_real,0)

global.__sort_sort=external_define(sDll,"sort",dll_cdecl,ty_real,1,ty_real)
global.__sort_get_string=external_define(sDll,"getString",dll_cdecl,ty_string,1,ty_real)
global.__sort_get_value=external_define(sDll,"getValue",dll_cdecl,ty_real,1,ty_real)

return external_call(global.__sort_init)
