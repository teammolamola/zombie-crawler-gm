/// cos_lerp(min,max,mix)
return argument1+(argument0-argument1)*(1+cos(argument2*pi))*0.5
