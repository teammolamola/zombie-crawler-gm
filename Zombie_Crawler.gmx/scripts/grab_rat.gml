with(argument0){
play_audio_pitch(sdEatRat,3,0.8,1.2)
add_blood(x,y,4,10)
if instance_exists(oCam)add_blood_drag(x,y,point_direction(x,y,oCam.x,oCam.y))
add_blood_particles(x,y,3,5,0,0,2,360,360)

repeat(2){
instance_create(
view_wview[0]/2+random_range(-200,200),
400+random_range(-200,200),oScreenBlood)}

global.rageMode = true
oCam.alarm[1] = 120
grab=true
x=xstart}
