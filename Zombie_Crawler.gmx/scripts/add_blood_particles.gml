/// add_blood_particles(x,y,z,num,rollmin,yawmax,speed,rollmax,yawmax)
var roll,yaw;
repeat(argument3){
roll=degtorad(random_range(argument4,argument7))
yaw=degtorad(random_range(argument5,argument8))

t=instance_create(argument0,argument1,oBloodParticles)
t.z=argument2
t.hspeed=(cos(roll)-sin(yaw))*argument6
t.vspeed=(sin(roll)-sin(yaw))*argument6
t.zVel=cos(yaw)*argument6}
