/// draw_text_outline(x,y,string,linecolor,fillcolor,resolution,size)
{
    draw_set_color(argument3)
    var i,an;
    an=360/argument5
    for(i=0;i<argument5;i+=1){
        draw_text(
        argument0+lengthdir_x(argument6,i*an),
        argument1+lengthdir_y(argument6,i*an),
        argument2)
    }
    
    draw_set_color(argument4)
    draw_text(argument0,argument1,argument2)
}
