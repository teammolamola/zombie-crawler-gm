//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
//attribute vec3 in_Normal;                  // (x,y,z)     unused in this shader.
//attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec3 v_vPosition;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vTexcoord = in_TextureCoord;
    v_vPosition = gl_Position.xyz;
}

//######################_==_YOYO_SHADER_MARKER_==_######################@~//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec3 v_vPosition;

void main()
{
    float fog = (1.0 - clamp(v_vPosition.z * 0.006 - 0.2, 0.0, 1.0));
    vec4 col = texture2D( gm_BaseTexture, v_vTexcoord );

    if (col.a > 0.5)
        gl_FragColor = vec4(fog, fog, fog, 1.0) * vec4(col.rgb, 1.0);
    else
        discard;
}

