//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
attribute vec3 in_Normal;                    // (x,y,z)     unused in this shader.
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vGradPos;
varying vec3 v_vPosition;
varying vec3 v_vNormal;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vPosition = (gm_Matrices[MATRIX_WORLD] * object_space_pos).xyz;
    v_vGradPos = gl_Position;
    v_vColour = in_Colour;
    v_vTexcoord = in_TextureCoord;
    v_vNormal = (gm_Matrices[MATRIX_WORLD] * vec4(in_Normal,0.0)).xyz;
}

//######################_==_YOYO_SHADER_MARKER_==_######################@~//
// Simple passthrough fragment shader
//
uniform vec3 v_vLightPos;

varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vGradPos;
varying vec3 v_vPosition;
varying vec3 v_vNormal;

const vec4 mixCol1 = vec4(0.31, 0.35, 0.40, 1.0);
const vec4 mixCol2 = vec4(0.77, 0.69, 0.51, 1.0);

vec3 lighting(vec3 pos, vec3 col, vec3 norm, vec3 lpos, float lrange, vec3 lcol)
{
    vec3 N = normalize(lpos-pos);
    float L = max(1.0-length(pos-lpos)/lrange,0.0);

    return (col * lcol * L * pow(max(dot(norm,N),0.0),4.0));
}

void main()
{
    /*vec3 surfLight = normalize(v_vLightPos - v_vGradPos.xyz);
    vec3 norm = normalize(v_vNormal);
    
    vec3 surfView = normalize(v_vGradPos.xyz);
    vec3 ref = reflect(norm, surfLight);
    float scont = pow(max(0.0, dot(surfView, ref)), 5.0);*/
    
    float mixVal = clamp(v_vGradPos.y * 0.02 + 0.2, 0.0, 1.0);
    
    float fogVal = (1.0 - clamp(v_vGradPos.z * 0.006 - 0.2, 0.0, 1.0));
    
    vec3 spec = lighting(v_vPosition, vec3(1.0, 1.0, 1.0), v_vNormal, v_vLightPos, 7000.0, vec3(1.0, 1.0, 1.0)) + fogVal;
    
    vec4 col = mix(mixCol1, mixCol2, mixVal) * vec4(spec, 1.0);
    gl_FragColor = texture2D(gm_BaseTexture, v_vTexcoord) * col;
}

