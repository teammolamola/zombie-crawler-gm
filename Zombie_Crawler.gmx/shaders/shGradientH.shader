//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
//attribute vec3 in_Normal;                  // (x,y,z)     unused in this shader.
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vGradPos;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vGradPos = gl_Position;
    v_vColour = in_Colour;
    v_vTexcoord = in_TextureCoord;
}

//######################_==_YOYO_SHADER_MARKER_==_######################@~//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vGradPos;

const vec4 mixCol1 = vec4(0.2823, 0.5686, 0.6039, 1.0);
const vec4 mixCol2 = vec4(0.7725, 0.6862, 0.5137, 1.0);

void main()
{
    vec4 col = mix(mixCol1, mixCol2, v_vGradPos.y * 0.006 - 0.6);
    col.r = clamp(col.r, 0.0, 1.0);
    col.g = clamp(col.g, 0.0, 1.0);
    col.b = clamp(col.b, 0.0, 1.0);
    gl_FragColor = texture2D(gm_BaseTexture, v_vTexcoord) * col;
}

