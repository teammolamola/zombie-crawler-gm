//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
//attribute vec3 in_Normal;                  // (x,y,z)     unused in this shader.	
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vColour = in_Colour;
    v_vTexcoord = in_TextureCoord;
}

//######################_==_YOYO_SHADER_MARKER_==_######################@~//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

float rand(vec2 co)
{
    return 1.0 - fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453) * 0.1;
}

void main()
{
    float dist = length(v_vTexcoord.xy - vec2(0.5));
    float vignette = smoothstep(1.0, 0.1, dist * 1.3);
    float d = clamp(vignette * 8.0, 0.0, 1.0);
    vec4 col = vec4(d, vignette, vignette, 1.0);

    float luma = dot(texture2D(gm_BaseTexture, v_vTexcoord).rgb, vec3(0.6, 0.3, 0.1)) * 0.7;
    luma += 0.15 * rand(v_vTexcoord);
    gl_FragColor = col * vec4(luma, luma, luma, 1.0);
}

